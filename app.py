""" created by XW """

from flask import Flask, render_template, json
import matplotlib.pyplot as plt
import mpld3
from mpld3 import plugins
import pandas as pd
import numpy as np
plt.style.use('ggplot')
css = """
body {color: white; background: black;}
div {text-align: center; font-size: 100%;}
"""

app = Flask(__name__)

# Define a function that will return an HTML snippet.
def build_plot():
    x_deets = list(np.random.random(10))
    y_deets = list(np.random.random(10))
    label = [(round(x, 3), round(y, 3)) for x, y in zip(x_deets, y_deets)]
    fig, ax = plt.subplots()
    lines = ax.plot(x_deets, y_deets, marker='.')
    plugins.connect(fig, plugins.MousePosition(fontsize=14))
    plugins.connect(fig, plugins.PointLabelTooltip(lines[0], label))
    output = json.dumps(mpld3.fig_to_dict(fig))
    return output

# Define our URLs and pages.
@app.route('/')
def render_plot():
    y = [3, 10, 7, 5, 3, 4.5, 6, 8.1]
    x = list(range(len(y)))
    
    fig, ax = plt.subplots()
    lines = ax.bar(x, y, 1/1.5, color = "blue")
    label = [(round(x, 3), round(y, 3)) for x, y in zip(x, y)]
    plugins.connect(fig, plugins.MousePosition(fontsize=14))
    i = 0
    for line in lines:
        plugins.connect(fig, plugins.PointLabelTooltip(line, [label[i]]))
        i += 1
    output = json.dumps(mpld3.fig_to_dict(fig))
    sample_list = list(np.random.randint(1, 99999999, size=10))
    dict_of_plots = list()
    for i in sample_list:
        single_chart = dict()
        single_chart['id'] = 'figs_' + str(i)
        single_chart['json'] = build_plot()
        dict_of_plots.append(single_chart)
    single_chart = dict()
    single_chart['id'] = 'figure_test'
    single_chart['json'] = output
    dict_of_plots.append(single_chart)
    return render_template('plots.html', dict_of_plots=dict_of_plots)#snippet=plot_snippet)

if __name__ == '__main__':
    app.run(debug=True)
