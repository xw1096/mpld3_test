import numpy as np
import matplotlib.pyplot as plt
import mpld3

fig, ax = plt.subplots()
x = list(range(10))
y = list(range(10,20))
lines = ax.plot(x,y, marker='o')
mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(lines[0],labels=y))

mpld3.save_html(fig,'asd.html')